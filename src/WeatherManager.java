import java.io.*;
import java.util.Scanner;

public class WeatherManager {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Write path");
            FileReader fileReader = new FileReader(scanner.nextLine());
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            FileWriter fileWriter = new FileWriter("weatherInBasel.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (int i = 0; i < 10; i++) {
                bufferedReader.readLine();
            }
            double averageTemperature = 0;
            double averageHumidity = 0;
            double averageWindSpeed = 0;
            char[] timeMaxTemperature = new char[13];
            char[] timeMinHumidity = new char[13];
            char[] timeMaxWindSpeed = new char[13];
            double maxTemperature = Double.MIN_VALUE;
            double minHumidity = Double.MAX_VALUE;
            double maxWindSpeed = Double.MIN_VALUE;
            int[] countOfDirections = new int[4];
            for (int i = 0; i < 192; i++) {
                String[] s = bufferedReader.readLine().split(",");
                averageTemperature += Double.parseDouble(s[1]);
                averageHumidity += Double.parseDouble(s[2]);
                averageWindSpeed += Double.parseDouble(s[3]);
                if (maxTemperature < Double.parseDouble((s[1]))) {
                    maxTemperature = Double.parseDouble(s[1]);
                    timeMaxTemperature = s[0].toCharArray();
                }
                if (minHumidity > Double.parseDouble(s[2])) {
                    minHumidity = Double.parseDouble(s[2]);
                    timeMinHumidity = s[0].toCharArray();
                }
                if (maxWindSpeed < Double.parseDouble(s[3])) {
                    maxWindSpeed = Double.parseDouble(s[3]);
                    timeMaxWindSpeed = s[0].toCharArray();
                }
                if (Double.parseDouble(s[4]) >= 315 || Double.parseDouble(s[4]) < 45) {
                    countOfDirections[0]++;
                }
                if (Double.parseDouble(s[4]) >= 45 || Double.parseDouble(s[4]) < 135) {
                    countOfDirections[1]++;
                }
                if (Double.parseDouble(s[4]) >= 135 || Double.parseDouble(s[4]) < 225) {
                    countOfDirections[2]++;
                }
                if (Double.parseDouble(s[4]) >= 225 || Double.parseDouble(s[4]) < 315) {
                    countOfDirections[3]++;
                }
            }
            averageTemperature /= 192;
            averageHumidity /= 192;
            averageWindSpeed /= 192;
            bufferedReader.close();
            bufferedWriter.write("Average temperature: " + averageTemperature);
            bufferedWriter.newLine();
            bufferedWriter.write("Average humidity: " + averageHumidity);
            bufferedWriter.newLine();
            bufferedWriter.write("Average wind speed: " + averageWindSpeed);
            bufferedWriter.newLine();
            bufferedWriter.write("The time when the temperature was maximum: " +
                    createReadableTime(timeMaxTemperature));
            bufferedWriter.newLine();
            bufferedWriter.write("The time when the humidity was minimum: " +
                    createReadableTime(timeMinHumidity));
            bufferedWriter.newLine();
            bufferedWriter.write("The time when the wind speed was maximum: " +
                    createReadableTime(timeMaxWindSpeed));
            bufferedWriter.newLine();
            bufferedWriter.write("Most frequent wind direction: " + writeWindDirection(countOfDirections));
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String writeWindDirection(int[] countOfDirections) {
        int max = Integer.MAX_VALUE;
        int iMax = 0;
        for (int i = 0; i < countOfDirections.length; i++) {
            if (countOfDirections[i] < max) {
                max = countOfDirections[i];
                iMax = i;
            }
        }
        String direction = "";
        switch (iMax) {
            case 0:
                direction = "North";
                break;
            case 1:
                direction = "East";
                break;
            case 2:
                direction = "South";
            break;
            case 3:
                direction = "West";
            break;
        }
        return direction;
    }

    public static String createReadableTime(char[] time) {
        return "date " + time[6] + time[7] + "." + time[4] + time[5] + "." + time[0] + time[1] + time[2] + time[3] +
                " time " + time[9] + time[10] + ":" + time[11] + time[12];
    }
}
